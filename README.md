# A simple host-apd sample

This project explains about a simple way to make your linux drive into an Access Point. Host-apd will enable you to fix the channel, transmission power, encryption and ... of the accesspoint the way you desire.

## Getting Started

The first step is to make sure that your WiFi card supports Accesspoint(AP) Mode. to do so, run the following command in your terminal (make sure to install iw if you already have not done so):

```
iw list
```
After running this command, under the "Supported interface modes" section almost in the begining of the results that come up you should see "AP".

Then you should install host apd:

```
sudo apt-get install hostapd
```
After installation, you should configure a script which basically enables you to twist the accesspoint the way you desire. The configuration file is located here:

```
/etc/hostapd/hostapd.conf
```

A simple example of such file is like what follows:

```
interface=wlan0        # it means that wlan0 virutal interface is going to be used as the accesspoint
ssid=hostapd-sample    # name of SSID that will be shown in other devices
channel=6              # the accesspoint will run on channel 6 and 2.4 
hw_mode=g              # the accesspoint will work at 2.4

ieee80211n=1           # just in case you want to add 802.11n support. Otherwise, it will just support 11g

```

Then you should run the host-apd using the following command:
```
sudo /usr/sbin/hostapd /etc/hostapd/hostapd.conf
```

Then if everything is setup correctly, you should see an accesspoint with SSID of "hostapd-helloworld" by any of your devices searching for WiFi connection like your phone or laptop.

## Considerations to avoid any problem

* make sure that you are using same ip address range for the wlan that you are configuring in the configuration file in the (.conf file). To change the ip address of an interface, simply use this command:

```
ifconfig <name of the interface(for instance wlan0)> ip <desired ip address (for example 10.0.0.1)>
```

* You should also change the ip address of the client which is going to connect to your host-apd based accesspoint. As by default no DHCP server is running on your accesspoint, this phase is necessary.

* There is an important concept that is the fact that there are some physical interfaces that some virutal interfaces are built on top of them. physical interfaces can be seen and observed by "iw list" it should show the WiFi physical interfaces by term "Wiphy phy0" or something like that. On top of these physical devices some virtual interfaces are built and called "wlan0" or names like that. To observe and manage them any of the following commands can be used:

```
ip link show
```

```
Ifconfig -a
```

* The virtual interfaces can be shut down or turn on. To turn them on any of the following commands can be used (to turn off "up" will be substituted by "down"):
```
sudo ifconfig wlan0 up
```

```
ifup wlan0
```

* To see if a virtual interface (for instance "wlan0") is up or down use the following command:

```
sudo iwlist wlan0 scan
```

* Virtual interfaces might be busy by other processes. To make sure that no process is using a specific virtual interface, the following command is handy:

```
airmon-ng check kill
```

* You might seem to find some errors like "unknown hw_mode 'g" or something like that for the ssid. In that case the problem might have been because of the fact that you have copied the code from somewhere where like windows platform hwere some ^M is added to the file buy it is not being obsereved by you. It is recommended to type the whole configuration file using vim in linux.

* To set the txpower of the virtual interface that it is being used iw commands can be used. To do so, either of the following commands are handy:

```
iw dev <wlan0> set txpower <auto|fixed|limit> [tx power in dbm]
```

Or if you want to set the txpower of a physical interface like "phy0" you can use the following command:

```
iw phy <phy0> set txpower <auto|fixed|limit> [tx power in dbm]
```

In these commands, phy is pointing to a physical interface and dev is pointing to a virutal one. Also term "auto" is refering to the fact that using this the amount you are setting might be effective or it might not be effective. If you set it into fixed, this amount will be used without any other consideration. If you set it to limit it means it will be the maximum.

* For setting the physical bitrates of the interface, you can use a command like this:

```
iw wlan-2400mhz set bitrates legacy-2.4 ht-mcs-2.4 15 vht-mcs-2.4 sgi-2.4
```


* For further debugging, it might be nessesarry to know what physical antennas are in use. to do so you can use the following command:

```
iw <wlan0> station dump
```

Using this command, sending power of each of the physical wifi antennas will be shown. Then you can infere which is connected and which one is probably not connected. In the case of being not connected, the tx pwoer of the antenna should be shown as something around -60 dbm. Or at least having a significant difference with other antennas. Not that if your device is a sender, the device might just trust on sending data over the first one/two antennas and in case you have not connected any of those, your bitrate will decrease significantly. On the contrary, in the receiving side, if any of the antennas are connected. The maximum throughput will be achieved using maximum number of antennas available.

* For getting to know that in whcih channel the interface is sending data the following iw command is usable:


```
iw <wlan0> info
```

* to set the power management mode of a wireless interface off use this command:

```
sudo iwconfig <wlan0> power off
```


# Good links:

airmon-ng:(https://www.aircrack-ng.org/doku.php?id=airmon-ng)

iw:(https://wireless.wiki.kernel.org/en/users/documentation/iw)

ip:(https://wireless.wiki.kernel.org/en/users/documentation/iw)

ifconfig :(https://linux.die.net/man/8/ifconfig)

